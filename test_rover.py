from rover import Rover

def test_rover_exists():

    r = Rover(0, 0)
    assert r.x == 0
    assert r.y == 0
    assert r.facing == 'n'

def test_move():

    r = Rover()
    r.x == 0
    r.y == 0
    r.facing = 'n'

    r.move('mf')
    assert r.x == 0
    assert r.y == 1


    r.move('mb')
    assert r.x == 0
    assert r.y == 0

    r.move('rr')
    assert r.facing == 'e'

    r.move('rr')
    assert r.facing == 's'

    r.move('rr')
    assert r.facing == 'w'

    r.move('rl')
    assert r.facing == 'n'





def test_commands():
    r = Rover()
    r.commands({'mf','mb', 'rr', 'rl'})

    assert r.x == 4
    assert r.y == 4