class Rover:

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        self.facing == 'n'

    def move(self, direction):

        if self.facing == 'n' and direction =='f':
            self.y +=1

        elif self.facing == 'e' and direction =='f':
            self.x +=1

        elif self.facing == 'n' and direction =='f':
            self.y +=1

        elif self.facing == 'e' and direction =='f':
            self.x +=1



    def rotation(self, direction):
        rotations ={'r': {'n':'e', 'e':'s', 's':'w', 'w':'n' },
            'l' : {'n':'w', 'w':'s', 's':'e', 'e':'n'}}

        self.facing = rotations[direction][self.facing]

    def commands(self, commands_list):
        for c in commands_list:
            if c[0] == 'm':
                self.move(c[1])

            elif c[0] == 'r':
                self.rotate(c[1])